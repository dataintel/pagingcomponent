import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseWidgetPagingComponent } from './src/base-widget.component';
import { MaterialModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

export * from './src/base-widget.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    BrowserAnimationsModule
  ],
  declarations: [
	BaseWidgetPagingComponent
  ],
  exports: [
	BaseWidgetPagingComponent
  ]
})
export class pagingModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: pagingModule,
      providers: []
    };
  }
}
